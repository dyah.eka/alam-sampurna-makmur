import { flushPromises, mount } from "@vue/test-utils"
import App from "@/App.vue"
import router from "@/router"

describe("App.vue", () => {
    it("routing Login -> Take Queue -> Queue System", async () => {
        router.push("/")

        await router.isReady()

        const wrapper = mount(App, {
            global: {
                plugins: [router]
            }
        })

        expect(wrapper.findComponent('Login')).toBeDefined()
        expect(wrapper.html()).toContain('Silakan "Masuk" untuk memulai perkerjaan!')
        await wrapper.find('form').trigger('submit')

        await flushPromises()

        expect(wrapper.findComponent('Take Queue')).toBeDefined()
        expect(wrapper.html()).toContain('Silakan tekan tombol untuk mengambil nomor antrean Anda')
        await wrapper.find('button').trigger('click')

        await flushPromises()

        expect(wrapper.findComponent('Queue System')).toBeDefined()
        expect(wrapper.html()).toContain('Antrian Selesai Proses')
    })
})