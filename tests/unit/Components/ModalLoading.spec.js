import { mount } from "@vue/test-utils";
import ModalLoading from "@/components/Modals/ModalLoading.vue";

describe("ModalLoading.vue", () => {
    const wrapper = mount(ModalLoading)

    it("modal show and didn't show as expected", async () => {
        await wrapper.setProps({ showModal: false })
        expect(wrapper.isVisible()).toBe(false)

        await wrapper.setProps({ showModal: true })
        expect(wrapper.isVisible()).toBe(true)
    })
})