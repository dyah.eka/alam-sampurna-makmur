import { mount } from "@vue/test-utils";
import InputCustom from "@/components/InputCustom/InputCustom.vue";

describe("InputCustom.vue", () => {
    const wrapper = mount(InputCustom);

    it("has props", () => {
        expect(InputCustom.props).toBeDefined();
    })
    it("has data showPassword", () => {
        const showPassword = { "showPassword": false }
        expect(InputCustom.data()).toEqual(showPassword)
    })
    it("set value on input element", async () => {
        const input = wrapper.get('input');

        await input.setValue("username")

        expect(input.element.value).toBe("username")
    })
    it("password won't show by default", async () => {
        await wrapper.setProps({ type: 'password' })
        expect(wrapper.html()).toContain('type="password"')
    })
    it("show/hide password when click on the eye symbol", async () => {
        await wrapper.setData({ showPassword: false })
        await wrapper.setProps({ type: 'password' })
        
        const eyeSymbol = wrapper.get("div.absolute.right-4.top-2")
        const input = wrapper.get('input');

        await eyeSymbol.trigger('click');
        expect(input.html()).toContain('type="text"')

        await eyeSymbol.trigger('click');
        expect(input.html()).toContain('type="password"')
    })
})