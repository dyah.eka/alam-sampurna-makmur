import { mount } from "@vue/test-utils";
import Modal from "@/components/Modals/Modal.vue";

describe("Modal.vue", () => {
    it("slots rendered", () => {
        const wrapper = mount(Modal, {
            slots: {
                header: "<div>Header</div>",
                main: "<div>Main</div>",
                footer: "<div>Footer</div>"
            }
        })
        expect(wrapper.html()).toContain("<div>Header</div>")
        expect(wrapper.html()).toContain("<div>Main</div>")
        expect(wrapper.html()).toContain("<div>Footer</div>")
    })

    const wrapper = mount(Modal)

    it("modal show and didn't show as expected", async () => {
        await wrapper.setProps({ show: false })
        expect(wrapper.isVisible()).toBe(false)

        await wrapper.setProps({ show: true })
        expect(wrapper.isVisible()).toBe(true)
    })
    it("emitted toggle event when outer div (backdrop) was clicked", async () => {
        await wrapper.find('div').trigger('click');
        expect(wrapper.emitted()).toHaveProperty('toggle')
    })
})