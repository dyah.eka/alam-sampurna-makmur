import { mount } from "@vue/test-utils";
import ModalConfig from "@/components/Modals/ModalConfig.vue";

describe("ModalConfig.vue", () => {
    const wrapper = mount(ModalConfig)

    it("modal show and didn't show as expected", async () => {
        await wrapper.setProps({ showModal: false })
        expect(wrapper.isVisible()).toBe(false)

        await wrapper.setProps({ showModal: true })
        expect(wrapper.isVisible()).toBe(true)
    })
    it("click button Batal will emitted close event", async () => {
        await wrapper.find('button').trigger('click')

        expect(wrapper.emitted()).toHaveProperty('close')
    })
    it("click button Simpan will emitted save event", async () => {
        await wrapper.get('button[type="submit"]').trigger('click')

        expect(wrapper.emitted()).toHaveProperty('save')
    })
})