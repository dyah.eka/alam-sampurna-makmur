import { mount } from "@vue/test-utils"
import SectionQueue from "@/views/QueueSystem/SectionQueue/SectionQueue.vue"

describe("SectionQueue.vue", () => {
    it("passing data to child components", () => {
        const wrapper = mount(SectionQueue)
        
        const lokets = wrapper.getComponent({name: 'Lokets'})
        expect(lokets.props().loketList).toBeDefined()
        expect(lokets.props().loketList).toStrictEqual(SectionQueue.data().lokets)

        const queuesComplete = wrapper.getComponent({name: 'QueuesComplete'})
        expect(queuesComplete.props().numbers).toBeDefined()
        expect(queuesComplete.props().numbers).toStrictEqual(SectionQueue.data().complete)
    })
})