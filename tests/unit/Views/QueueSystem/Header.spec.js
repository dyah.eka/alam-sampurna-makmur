import { mount } from "@vue/test-utils"
import Header from "@/views/QueueSystem/Header/Header.vue"

const timeToStr = (datetime) => {
    return datetime.toLocaleTimeString("en-US", {
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        hours12: true,
    }).replace(/:/g, " : ")
}

const dateToStr = (datetime) => {
    return datetime.toLocaleString("id", {
        weekday: "long",
        day: "numeric",
        month: "long",
        year: "numeric",
    });
}

describe("Header.vue", () => {
    
    it("call getDatetime method when mounted", async () => {
        const mockMethod = jest.spyOn(Header.methods, 'getDatetime')

        await mount(Header)
        expect(mockMethod).toHaveBeenCalled()
    })
    it("ticking 5 times and 12 times", () => {
        jest.useFakeTimers();
        
        const wrapper= mount(Header)

        jest.advanceTimersByTime(5000)
        expect(wrapper.vm.$data.called).toBe(5)

        jest.advanceTimersToNextTimer(7)
        expect(wrapper.vm.$data.called).toBe(12)
    })
    it("renders actual date and time", async () => {
        jest.useFakeTimers();

        const wrapper = mount(Header)

        const datetime = new Date();

        const timeStr = timeToStr(datetime)
        const dateStr = dateToStr(datetime)
        
        const time = wrapper.get('p.text-base')
        const date = wrapper.get('p.text-xs.text-shades-orange-light')

        jest.runOnlyPendingTimers()
        
        await expect(wrapper.vm.$data.called).toBe(1)

        expect(time.text()).toBe(timeStr)
        expect(date.text()).toBe(dateStr)

        jest.clearAllTimers()
    })
    it("renders fake date and time with ticking 3 times", async () => {        
        const mockDate = new Date("2020/01/12")
        const _Date = Date
        global.Date = jest.fn(() => mockDate)
        global.Date.now = _Date.now
                
        const wrapper = mount(Header)

        let timeStr = timeToStr(mockDate)
        let dateStr = dateToStr(mockDate)

        const time = wrapper.get('p.text-base')
        const date = wrapper.get('p.text-xs.text-shades-orange-light')
        
        jest.runOnlyPendingTimers()

        await expect(wrapper.vm.$data.called).toBe(1)
        expect(time.text()).toEqual(timeStr)
        expect(date.text()).toEqual(dateStr)

        mockDate.setSeconds(mockDate.getSeconds() + 1)

        timeStr = timeToStr(mockDate)
        dateStr = dateToStr(mockDate)
        
        jest.runOnlyPendingTimers()

        await expect(wrapper.vm.$data.called).toBe(2)
        expect(time.text()).toEqual(timeStr)
        expect(date.text()).toEqual(dateStr)

        mockDate.setSeconds(mockDate.getSeconds() + 1)

        timeStr = timeToStr(mockDate)
        dateStr = dateToStr(mockDate)
        
        jest.runOnlyPendingTimers()

        await expect(wrapper.vm.$data.called).toBe(3)
        expect(time.text()).toEqual(timeStr)
        expect(date.text()).toEqual(dateStr)

        expect(wrapper.vm.$data.called).toBe(3)

        mockDate.setSeconds(mockDate.getSeconds() + 1)

        timeStr = timeToStr(mockDate)
        dateStr = dateToStr(mockDate)
        
        jest.runOnlyPendingTimers()

        await expect(wrapper.vm.$data.called).toBe(4)
        expect(time.text()).toEqual(timeStr)
        expect(date.text()).toEqual(dateStr)

        jest.clearAllTimers()
    })
})