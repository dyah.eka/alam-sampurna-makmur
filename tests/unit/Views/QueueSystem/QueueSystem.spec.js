import { mount } from "@vue/test-utils"
import QueueSystem from "@/views/QueueSystem/QueueSystem.vue"

describe("QueueSystem.vue", () => {
    let wrapper;

    beforeAll(() => {
        wrapper = mount(QueueSystem)
    })

    afterAll(() => {
        wrapper.unmount()
    })

    it("child components rendered", () => {
        expect(wrapper.getComponent({name: 'Header'})).toBeTruthy()
        expect(wrapper.getComponent({name: 'SectionQueue'})).toBeTruthy()
        expect(wrapper.getComponent({name: 'Footer'})).toBeTruthy()
        expect(wrapper.getComponent({name: 'ModalConfig'})).toBeTruthy()
        expect(wrapper.getComponent({name: 'ModalLoading'})).toBeTruthy()
    })
    it("shows ModalConfig when clicking on configuration symbol on Footer", async () => {
        const footer = wrapper.getComponent({name: 'Footer'})

        await footer.trigger('config')
        expect(wrapper.getComponent({name: 'ModalConfig'}).isVisible()).toBeTruthy()
    })
    it("Batal button will close ModalConfig, Simpan button will open ModalLoading", async () => {
        const modalConfig = wrapper.getComponent({name: 'ModalConfig'})
        const modalLoading = wrapper.getComponent({name: 'ModalLoading'})

        expect(modalLoading.isVisible()).toBeFalsy()
        
        await wrapper.setData({ showModalConfig: true })
        expect(modalConfig.isVisible()).toBeTruthy()

        await modalConfig.trigger('close')
        expect(modalConfig.isVisible()).toBeFalsy()

        await wrapper.setData({ showModalConfig: true })
        expect(modalConfig.isVisible()).toBeTruthy()

        await modalConfig.trigger('save')
        expect(modalConfig.isVisible()).toBeFalsy()
        expect(modalLoading.isVisible()).toBeTruthy()

    })
    it("ModalLoading will close by triggering toggle event", async () => {
        const modalLoading = wrapper.getComponent({name: 'ModalLoading'})

        await wrapper.setData({ showModalLoading: true })
        expect(modalLoading.isVisible()).toBeTruthy()

        await modalLoading.trigger('toggle')
        expect(modalLoading.isVisible()).toBeFalsy()
    })
})