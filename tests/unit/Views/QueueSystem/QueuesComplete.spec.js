import { mount } from "@vue/test-utils"
import QueuesComplete from "@/views/QueueSystem/SectionQueue/QueuesComplete.vue"

describe("QueuesComplete.vue", () => {
    it("renders the props", () => {
        const title = "Antrian Selesai",
            numbers = [1,2,3]

        const wrapper = mount(QueuesComplete, {
            props: {
                title,
                numbers
            }
        })

        expect(wrapper.get('p.font-bold').text()).toEqual(title)
        expect(wrapper.findAll('[data-test="number"]')).toHaveLength(3)
    })
})