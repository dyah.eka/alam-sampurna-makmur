import { mount } from "@vue/test-utils"
import Lokets from "@/views/QueueSystem/SectionQueue/Lokets.vue"

describe("Lokets.vue", () => {
    it("renders the loketList props with length 2", () => {
        const loketList = [
            {
                id: 1,
                title: "loket 1",
                person: "Trashae Hubbard",
                queueNumber: "14",
            },
            {
                id: 2,
                title: "loket 2",
                person: "Shirline Dungey",
                queueNumber: "19",
            },
        ]

        const wrapper = mount(Lokets, {
            props: {
                loketList
            }
        })

        expect(wrapper.findAllComponents({ name: 'LoketCard' })).toHaveLength(2)
    })
    it("renders the loketList props with length 3", () => {
        const loketList = [
            {
                id: 1,
                title: "loket 1",
                person: "Trashae Hubbard",
                queueNumber: "14",
            },
            {
                id: 2,
                title: "loket 2",
                person: "Shirline Dungey",
                queueNumber: "19",
            },
            {
                id: 3,
                title: "loket 3",
                person: "Zeng Wen",
                queueNumber: "17",
            },
        ]

        const wrapper = mount(Lokets, {
            props: {
                loketList
            }
        })

        expect(wrapper.findAllComponents({ name: 'LoketCard' })).toHaveLength(3)
    })
})