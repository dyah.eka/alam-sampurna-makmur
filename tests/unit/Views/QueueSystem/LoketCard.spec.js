import { mount } from "@vue/test-utils"
import LoketCard from "@/views/QueueSystem/SectionQueue/LoketCard.vue"

describe("LoketCard.vue", () => {
    it("renders the props", () => {
        const title = "Loket 1",
            person = "Pomerai Anjou",
            number = "2"

        const wrapper = mount(LoketCard, {
            props: {
                title,
                person,
                number
            }
        })

        const html = wrapper.html();

        expect(html).toContain(title)
        expect(html).toContain(person)
        expect(html).toContain(number)
    })
})