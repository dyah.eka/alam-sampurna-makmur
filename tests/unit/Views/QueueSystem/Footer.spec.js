import { mount } from "@vue/test-utils"
import Footer from "@/views/QueueSystem/Footer/Footer.vue"

describe("Footer.vue", () => {
    const wrapper  = mount(Footer);

    it("emitted config event when configuration symbol clicked", async () => {
        const getConfigSymbol = wrapper.get('div.float-left.flex.items-center')

        await getConfigSymbol.trigger('click')

        expect(wrapper.emitted()).toHaveProperty('config')
    })
})