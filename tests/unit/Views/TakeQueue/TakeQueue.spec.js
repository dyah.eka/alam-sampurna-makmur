import { mount } from "@vue/test-utils"
import TakeQueue from "@/views/TakeQueue/TakeQueue.vue"

describe("TakeQueue.vue", () => {
    const mockRouter = {
        push: jest.fn()
    }

    let wrapper;
    beforeAll(() => {
        wrapper = mount(TakeQueue, {
            global: {
                mocks: {
                    $router: mockRouter
                }
            }
        })
    })
    afterAll(() => {
        wrapper.unmount()
    })

    it("redirect to new page when button clicked", async () => {
        const button = wrapper.find('button')

        await button.trigger('click')
        expect(mockRouter.push).toHaveBeenCalledTimes(1)
        expect(mockRouter.push).toHaveBeenCalledWith({ name: "Queue System" })
    })
    
})