import { mount } from "@vue/test-utils"
import Login from "@/views/Login/Login.vue"

describe("Login.vue", () => {
    const mockRouter = {
        push: jest.fn()
    }

    let wrapper;
    beforeAll(() => {
        wrapper = mount(Login, {
            global: {
                mocks: {
                    $router: mockRouter
                }
            }
        })
    })
    afterAll(() => {
        wrapper.unmount()
    })

    it("renders child components", async () => {
        expect(wrapper.getComponent({name: 'FormLogin'})).toBeDefined()
    })
    it("redirect to new page when triggering redirect event", async () => {
        const formLogin = wrapper.getComponent({name: 'FormLogin'})

        await formLogin.trigger('redirect')
        expect(mockRouter.push).toHaveBeenCalledTimes(1)
        expect(mockRouter.push).toHaveBeenCalledWith({"name": "Take Queue"})
    })
    
})