import { mount } from "@vue/test-utils"
import FormLogin from "@/views/Login/FormLogin.vue"

describe("FormLogin.vue", () => {
    const wrapper = mount(FormLogin)

    it("emitted redirect event when submit clicked", async () => {
        const form = wrapper.get('form')

        await form.trigger('submit')

        expect(wrapper.emitted()).toHaveProperty('redirect')
    })
    it("filled form with username and password", async () => {
        const username = "john_doe"
        const password = "admin123"

        await wrapper.get('input[name="username"]').setValue(username)
        await wrapper.get('input[name="password"]').setValue(password)

        expect(wrapper.get('input[name="username"]').element.value).toEqual(username)
        expect(wrapper.get('input[name="password"]').element.value).toEqual(password)

    })
})