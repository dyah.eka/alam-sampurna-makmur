module.exports = {
	purge: [],
	theme: {
		// gradients: () => ({
		//   blues: ['#2071ee', '#44c4f1'],
		//   'blues-secondary': ['to left', '#44c4f1', '#2071ee']
		// }),
		fontFamily: {
			montserrat: ["Montserrat"],
		},
		extend: {
			colors: {
				"app-primary": "#f1b102",
				"app-secondary": "#44c4f1",
				"app-alt": "#99daff",
				"app-base": "#ffffff",
				"status-pending": "#ffa800",
				"status-approved": "#13ce66",
				"list-background": "#f5faff",
				"label-color": "#57585a",
				"shades-blue": "#1f75ff",
				"shades-blue-light": "#b5e4ff",
				"shades-blue-lighter": "#daeafb",
				"shades-black": "#232323",
				"shades-grey": "#6f6f6f",
				"shades-orange-light": "#FA6400",
				"obsidian-transparent": "RGBA(0,0,0,0.1)",
				"obsidian-transparent-51": "RGBA(0,0,0,0.51)",
				"calcite-transparent": "RGBA(255,255,255,0.2)",
				"light-grey": "#EFEFEF"
			},
			screens: {
				print: { raw: "print" },
				"lg-mid": { max: "1196px" },
				xs: { max: "450px" },
				sm: { max: "600px" }
			},
			margin: {
				"15": "3.75rem",
			},
			width: {
				"50": "12.5rem"
			}
		},
		textColor: (theme) => ({
			...theme("colors"),
		}),
		backgroundColor: (theme) => ({
			...theme("colors"),
		}),
	},
	variants: {
		backgroundColor: ["odd", "hover"],
		gradients: ["responsive", "hover"],
	},
	plugins: [],
};
