import { createRouter, createWebHistory } from "vue-router";
import Login from "../views/Login/Login.vue";
// import TakeQueue from "../views/TakeQueue/TakeQueue.vue";
// import QueueSystem from "../views/QueueSystem/QueueSystem.vue";

const routes = [
	{
		path: "/",
		name: "Login",
		component: Login,
	},
	{
		path: "/take-queue",
		name: "Take Queue",
		component: () => import("../views/TakeQueue/TakeQueue.vue"),
	},
	{
		path: "/queue-system",
		name: "Queue System",
		component: () => import("../views/QueueSystem/QueueSystem.vue"),
	},
	{
		path: "/:pathMatch(.*)",
		redirect: "/",
	},
];

const router = createRouter({
	history: createWebHistory(process.env.BASE_URL),
	routes,
});

export default router;
