# alam-sampurna-makmur

## URL
[Alam Sampurna Makmur](https://alam-sampurna-makmur.netlify.app)

Klik Masuk pada halaman Login akan diarahkan ke halaman Ambil Antrian.
Klik Ambil Antrian pada halaman Ambil Antrian akan diarahkan ke halaman Sistem Antrian.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
